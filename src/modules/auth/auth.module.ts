import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { EnvService } from '../../configurations/env.service';
import { ConfService } from 'src/configurations/conf.service';
import { ConfigurationModule } from '../../configurations/configurations.module';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { PrismaModule } from 'src/database/prisma.module';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { JwtAuthGuard } from 'src/core';

@Module({
  imports: [
    ConfigurationModule,
    PrismaModule,
    // PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.registerAsync({
      inject: [ConfService],
      useFactory: async (configService: ConfService) => {
        let jwtConfigs = await configService.getJwtConfigs()
        const jwtConfig = {
          secret: jwtConfigs.JWT_SECRET,
          signOptions: {
            expiresIn: jwtConfigs.JWT_EXPIRE
          }
        }
        return jwtConfig;
      }
    }),
  ],
  controllers: [AuthController],
  // providers: [AuthService, JwtAuthGuard],
  providers: [AuthService],
})
export class AuthModule { }
