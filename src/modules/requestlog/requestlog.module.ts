import { Module } from '@nestjs/common';
import { RequestlogService } from './requestlog.service';
import { PrismaModule } from 'src/database/prisma.module';

@Module({
  controllers: [],
  providers: [RequestlogService],
  imports: [PrismaModule],
  exports: [RequestlogService],
})
export class RequestlogModule {}
