-- CreateTable
CREATE TABLE "requestlog" (
    "id" SERIAL NOT NULL,
    "url" TEXT,
    "method" TEXT,
    "statusCode" TEXT,
    "req" TEXT,
    "res" TEXT,
    "timestamp" TEXT,
    "createdAt" TIMESTAMPTZ(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMPTZ(3) NOT NULL,
    "deletedAt" TIMESTAMP(3),

    CONSTRAINT "requestlog_pkey" PRIMARY KEY ("id")
);
