/*
  Warnings:

  - The `statusCode` column on the `requestlog` table would be dropped and recreated. This will lead to data loss if there is data in the column.

*/
-- AlterTable
ALTER TABLE "requestlog" DROP COLUMN "statusCode",
ADD COLUMN     "statusCode" INTEGER;
