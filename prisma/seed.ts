import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient()
async function main() {
    await prisma.conf.createMany({
        data: [
            { name: 'JWT_SECRET', value: 'hihiih', type: 'string' },
            { name: 'JWT_EXPIRE', value: '70d', type: 'string' },
        ],
    });
    await prisma.user.createMany({
        data: [
            { phone: '0329611766', role: 'USER' },
        ],
    });
}

main().then(() => { console.log('Seeding completed') }).catch(error => {
    console.log("Seeding err: ", error)
})